#include <stdio.h>
#include <string.h>

struct Student
{
	int id;
	int marks;
	char favChar;
	char name[50];
};
struct Student sid, mahi; // -> global variables

int main()
{
	struct Student siddarth, mahesh; // -> local variables
	siddarth.id = 72;
	mahesh.id = 54;
	siddarth.marks = 81;
	mahesh.marks = 99;
	siddarth.favChar = 'A';
	mahesh.favChar = 'B';
	strcpy(siddarth.name, "Siddarth");
	printf();

	return 0;
}
