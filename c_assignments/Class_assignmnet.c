// Q1 NATURALS NUMBERS FROM 1 TO 100 IN REVERSE ORDER:-

// #include <stdio.h>
// int main()
// {
//     int n;
//     for (n = 100; n >= 1; n--)
//     {
//         printf("%d \n", n);
//     }
// }

// Q2 UPPER CASE AND LOWER CASE:-

// #include <stdio.h>
// int main()
// {
//     char ch;
//     printf("Please enter characters: \n");
//     printf("Enter '*' to quit : \n");
//     int upper_case = 0;
//     int lower_case = 0;
//     int digits;
//     while (ch != '*')
//     {
//         scanf("%c", &ch);
//         if (ch >= 'a' && ch <= 'z')
//         {
//             lower_case++;
//         }
//         else if (ch >= 'A' && ch <= 'Z')
//         {
//             upper_case++;
//         }
//         else if (ch >= 0 && ch != '*')
//         {
//             digits++;
//         }
//     }
//     printf("Uppercase = %d \n", upper_case);
//     printf("Lowercase = %d \n", lower_case);
//     printf("Total number of charecters = %d \n", digits);
// }

// Q3 MULTIPLIACTION TABLE OF AN INTEGER N:-

// #include <stdio.h>
// int main()
// {
//     int i, j;
//     printf("Enter any integer N : ");
//     scanf("%d", &i);
//     for (j = 1; j <= 10; j++)
//     {
//         printf("%d x %d = %d \n", i, j, i * j);
//     }
// }

// Q4 SUM OF DIGITS OF A NUMBER:-

// #include <stdio.h>
// int main()
// {
//     int n, r, add = 0;
//     printf("Enter the number : ");
//     scanf("%d", &n);
//     while (n != 0)
//     {
//         r = n % 10;
//         add += r;
//         n /= 10;
//     }
//     printf("Sum of the digits of the number is = %d ", add);
//     return 0;
// }

// Q5 ROOTS USING SWITCH CASE

// #include <stdio.h>
// #include <math.h>

// int main()
// {
//     int a, b, c;
//     printf("Enter the coefficients of quadrratic equation a*x*x + b*x + c \n");
//     printf("a: ");
//     scanf("%d", &a);
//     printf("b: ");
//     scanf("%d", &b);
//     printf("c: ");
//     scanf("%d", &c);

//     float d;
//     float r1, r2;
//     float im;

//     d = ((b * b) - (4 * a * c));

//     int arg;
//     if (d == 0)
//     {
//         arg = 1;
//     }
//     else if (d > 0)
//     {
//         arg = 2;
//     }
//     else
//     {
//         arg = 3;
//     }

//     switch (arg)
//     {
//     case 1:
//         printf("Real and equal roots \n");
//         r1 = r2 = -b / (2 * a);
//         printf("The roots are %.2f and %.2f \n", r1, r2);
//         break;
//     case 2:
//         printf("Real and distinct roots \n");
//         r1 = ((-b - sqrt(d)) / (2 * a));
//         r2 = ((-b + sqrt(d)) / (2 * a));
//         printf("The roots are %f and %f \n", r1, r2);
//         break;
//     case 3:
//         printf("Imaginary roots \n");
//         r1 = r2 = -b / (2 * a);
//         im = sqrt(-d) / (2 * a);
//         printf("The roots are %.2f + i%.2f and %.2f - i%.2f", r1, im, r2, im);
//         break;
//     }

//     return 0;
// }

// Q6 POSITIVE OR NEGATIVE USING SWITCH CASE

// #include <stdio.h>
// int main()
// {
//     int i;
//     printf("i: \n");
//     scanf("%d", &i);
//     int flag;

//     if (i > 0)
//     {
//         flag = 2;
//     }
//     else if (i < 0)
//     {
//         flag = 1;
//     }
//     else if (i == 0)
//     {
//         flag = 0;
//     }

//     switch (flag)
//     {
//     case 2:
//         printf("Positive \n");
//         break;
//     case 0:
//         printf("Neutral \n");
//         break;
//     case 1:
//         printf("Negative \n");
//         break;
//     }
//     return 0;
// }

//  Q7 ODD NUMBERS FROM 1 TO N:-

// #include <stdio.h>
// int main()
// {
//     int n, m;
//     printf("Print odd numbers till : ");
//     scanf("%d", &n);
//     printf("Odd numbers from 1 to %d are : \n", n);
//     for (m = 1; m <= n; m++)
//     {
//         if (m % 2 != 0)
//         {
//             printf("%d\n", m);
//         }
//     }
//     return 0;
// }

// Q8 DAYS OF A WEEK SWITCH CASE

// #include <stdio.h>
// int main()
// {
//     int d;
//     printf("Enter the number of day: ");
//     scanf("%d", &d);

//     switch (d)
//     {
//     case 1:
//         printf("SUNDAY \n");
//         break;
//     case 2:
//         printf("MONDAY \n");
//         break;
//     case 3:
//         printf("TUESDAY \n");
//         break;
//     case 4:
//         printf("WEDNESDAY \n");
//         break;
//     case 5:
//         printf("THURSDAY \n");
//         break;
//     case 6:
//         printf("FRIDAY \n");
//         break;
//     case 7:
//         printf("SATURDAY \n");
//         break;
//     default:
//         printf("Values only from 1 to 7 are accepted \n");
//     }
// }

// Q9 INSERTION OF ELEMENT IN ARRAY

// #include <stdio.h>
// int main()
// {
//     int i;
//     int arr[] = {1, 2, 3, 4, 5, 7, 8};
//     int n = 7;
//     int pos;
//     printf("Enter the position : ");
//     scanf("%d", &pos);
//     int val;
//     printf("Enter the value : ");
//     scanf("%d", &val);
//     for (i = n - 1; i >= pos - 1; i--)
//     {
//         arr[i] = arr[i + 1];
//     }
//     n++;
//     arr[pos - 1] = val;
//     for (i = 0; i < n; i++)
//     {
//         printf("%d \t", arr[i]);
//     }
// }

// Q10 DELETION OF AN ELEMENT IN ARRAY

// #include <stdio.h>
// int main()
// {
//     int arr[] = {2, 4, 6, 8, 9, 10, 12};
//     int pos, value, n, i;
//     n = 7;
//     printf("Enter the position : ");
//     scanf("%d", &pos);
//     printf("Enter the value : ");
//     scanf("%d", &value);
//     for (i = pos - 1; i < n - 1; i++)
//     {
//         arr[i] = arr[i + 1];
//     }
//     n--;
//     for (i = 0; i < n; i++)
//     {
//         printf("%d \t", arr[i]);
//     }
// }

// Q11 LINEAR SEARCH

// #include <stdio.h>
// int main()
// {
//     int n;
//     int i;
//     int arr[n];
//     printf("Enter no of elements : ");
//     scanf("%d", &n);
//     printf("Enter elements \n");
//     for (i = 0; i < n; i++)
//     {
//         scanf("%d", &arr[i]);
//     }
//     int k;
//     printf("Search for : ");
//     scanf("%d", &k);
//     for (i = 0; i < n; i++)
//     {
//         if (arr[i] == k)
//         {
//             printf("%d found at position %d ", k, i + 1);
//         }
//         else
//         {
//             printf("Cannot find %d", k);
//         }
//     }
// }

// Q12 BINARY SEARCH

// #include <stdio.h>
// int main()
// {
//     int i, low, high, mid, n, key, arr[100];
//     printf("Enter number of elements: ");
//     scanf("%d", &n);
//     printf("Enter %d integers \n", n);
//     for (i = 0; i < n; i++)
//         scanf("%d", &arr[i]);
//     printf("Search for: ");
//     scanf("%d", &key);
//     low = 0;
//     high = n - 1;
//     mid = (low + high) / 2;
//     while (low <= high)
//     {
//         if (arr[mid] == key)
//         {
//             printf("%d found at location %d \n", key, mid + 1);
//             break;
//         }
//         if (arr[mid] < key)
//         {
//             low = mid + 1;
//             mid = (low + high) / 2;
//         }
//         else
//         {
//             high = mid - 1;
//             mid = (low + high) / 2;
//         }
//     }
//     if (low > high)
//         printf("Cannot find %d in above array \n", key);
//     return 0;
// }

// #include <stdio.h>
// int main()
// {
//     int n;
//     int m;
//     int arr[100][100];
//     printf("No of salesman : ");
//     scanf("%d", &n);
//     printf("No of sales : ");
//     scanf("%d", &m);
//     for (int i = 0; i < n; i++)
//     {
//         for (int j = 0; j < m; j++)
//         {
//             printf("Enter the value for arr[%d][%d] : ", i, j);
//             scanf("%d", &arr[i][j]);
//         }
//         printf("\n");
//     }
//     for (int i = 0; i < n; i++)
//     {
//         for (int j = 0; j < m; j++)
//         {
//             printf("%d \t", arr[i][j]);
//         }
//         printf("\n");
//     }
//     int ind1 = 1;
//     for (int i = 0; i < n; i++)
//     {
//         int sum = 0;
//         for (int j = 0; j < m; j++)
//         {
//             sum += arr[i][j];
//         }
//         printf("Total sales of salesman %d = %d \n", ind1, sum);
//         ind1++;
//     }
//     printf("\n");
//     int ind2 = 1;
//     for (int i = 0; i < m; i++)
//     {
//         int sum = 0;
//         for (int j = 0; j < n; j++)
//         {
//             sum += arr[j][i];
//         }
//         printf("Total sales of the product %d = %d \n", ind2, sum);
//         ind2++;
//     }
//     return 0;
// }

