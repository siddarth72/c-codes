// Q ODD OR EVEN NUMBER

// #include <stdio.h>
// int main()
// {
//     int n;
//     printf("Enter the number: \n");
//     scanf("%d", &n);
//     if (n % 2 == 0)
//     {
//         printf("The number %d is even", n);
//     }
//     else
//     {
//         printf("The number %d is odd", n);
//     }
//     return 0;
// }

// Q TEMPERATURE CONVERSION

// #include <stdio.h>
// float temp_conv(float f);
// int main()
// {
//     float f, deg_cels;
//     printf("Temperature in degree Fahrenheit = \n");
//     scanf("%f", &f);
//     deg_cels = temp_conv(f);
//     printf("Temperature in degree Celsius = %f\n", deg_cels);
// }
// float temp_conv(float f)
// {
//     float c;
//     c = (((f - 31) * 5) / 9);
//     return c;
// }

// Q LOGICAL OPERATORS

// #include <stdio.h>
// int main()
// {
//     int a, b;
//     int res;
//     printf("Enter a: \n");
//     scanf("%d", &a);
//     printf("Enter b: \n");
//     scanf("%d", &b);
//     printf("a = %d\n", a);
//     printf("b = %d\n", b);
//     res = (a > b);
//     printf("For a > b the result is ");
//     printf("%d\n", res);
//     res = (a < b);
//     printf("For a < b result is ");
//     printf("%d\n", res);
//     res = (a == b);
//     printf("For a == b result is ");
//     printf("%d\n", res);
//     res = (a != b);
//     printf("For a != b result is ");
//     printf("%d\n", res);
//     res = ((a < b) && (a == b));
//     printf("For a < b && a == b result is ");
//     printf("%d\n", res);
//     res = ((a < b) || (a == b));
//     printf("For a < b || a == b result is ");
//     printf("%d\n", res);
//     res = (!(a < b));
//     printf("For !(a < b) result is ");
//     printf("%d\n", res);
//     return 0;
// }

// Q TIME IN MINIUTEW

// #include <stdio.h>
// int main()
// {
//     int minutes, hrs, mins;
//     printf("Time in minutes = ");
//     scanf("%d", &minutes);
//     hrs = (minutes / 60);
//     mins = (minutes - (hrs * 60));
//     printf("Hrs:Mins :: %d:%d\n", hrs, mins);
//     return 0;
// }

// Q DISTANCE BETWEEN TWO POINT IN COORDINATE SYSTEM

// #include <stdio.h>
// #include <math.h>
// int main()
// {
//     float x1, x2, y1, y2, dist;
//     printf("Enter 1st point (x1, y1)\n");
//     scanf("%f%f", &x1, &y1);
//     printf("Enter 2nd point (x2, y2)\n");
//     scanf("%f%f", &x2, &y2);
//     dist = sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
//     printf("Distance between (%0.2f, %0.2f) and (%0.2f, %0.2f) = %0.2f\n", x1, x2, y1, y2, dist);
//    return 0;
// }

// Q AVEARGE OF THREE NUMBERS

// #include <stdio.h>
// float avg(int a, int b, int c)
// {
//     float average;
//     average = ((a + b + c) / 3);
//     return average;
// }
// int main()
// {
//     int a, b, c;
//     printf("Enter 3 numbers: \n");
//     scanf("%d%d%d", &a, &b, &c);
//     float avrg;
//     avrg = avg(a, b, c);
//     printf("%f", avrg);
//     return 0;
// }

// Q AREA OG TRIANGLE

// #include <stdio.h>
// #include <math.h>
// double area_of_triangle(int a, int b, int c);
// int main()
// {
//     int a, b, c;
//     double area;
//     printf("Enter the sides of triangle: \n");
//     scanf("%d%d%d", &a, &b, &c);
//     area = area_of_triangle(a, b, c);
//     printf("The area of the triangle is = %.2lf", area);
//     return 0;
// }
// double area_of_triangle(int a, int b, int c)
// {
//     double s, ar;
//     double s1, s2, s3;
//     s = ((a + b + c) / 2);
//     s1 = s - a;
//     s2 = s - b;
//     s3 = s - c;
//     ar = sqrt(s * s1 * s2 * s3);
//     return ar;
// }

// Q ROOTS OF A QUADRATIC EQUATION

// #include <stdio.h>
// #include <math.h>
// int main()
// {
//     float a, b, c;
//     float x1, x2;
//     float d, im;
//     printf("a: ");
//     scanf("%f", &a);
//     printf("b: ");
//     scanf("%f", &b);
//     printf("c: ");
//     scanf("%f", &c);
//     d = (b * b) - (4 * a * c);
//     switch (d > 0)
//     {
//     case 1:
//         x1 = (-b + sqrt(d)) / (2 * a);
//         x2 = (-b - sqrt(d)) / (2 * a);
//         printf("Roots : %.2f and %.2f \n", x1, x2);
//         break;
//     case 0:
//         switch (d < 0)
//         {
//         case 1:
//             x1 = x2 = -b / (2 * a);
//             im = sqrt(-d) / (2 * a);
//             printf("roots : %.2f + i%.2f and %.2f - i%.2f \n", x1, im, x2, im);
//             break;
//         case 0:
//             x1 = x2 = -b / (2 * a);
//             printf("Roots : %.2f and %.2f \n", x1, x2);
//             break;
//         }
//         break;
//     }
//     return 0;
// }

// #include <stdio.h>
// #include <math.h>
// int main()
// {
//     int a, b, c, d;
//     double p, q;
//     printf("The quadratic equation is of the form a*x*x + b*x + c \n");
//     printf("Enter the three coefficients a, b and c: \n");
//     scanf("%d%d%d", &a, &b, &c);
//     d = b * b - 4 * a * c;
//     if (d < 0)
//     {
//         printf("The roots are imaginary \n");
//         printf("x1 = %.2lf + i%.2lf\n", -b / (double)(2 * a), sqrt(-d) / (2 * a));
//         printf("x2 = %.2lf - i%.2lf\n", -b / (double)(2 * a), sqrt(-d) / (2 * a));
//     }
//     else
//     {
//         printf("The roots are real \n");
//         p = (-b + sqrt(d)) / (2 * a);
//         q = (-b - sqrt(d)) / (2 * a);
//         printf("x1 = %.2lf\n", p);
//         printf("x2 = %.2lf\n", q);
//     }
//     return 0;
// }

// Q EVEN NUMBERS FROM M TO N

// #include <stdio.h>
// int main()
// {
//     int m, n;
//     int i = 0;
//     printf("m : ");
//     scanf("%d", &m);
//     printf("n : ");
//     scanf("%d", &n);
//     printf("Even number from %d to %d are\n", m, n);
//     for (i = m; i <= n; i++)
//     {
//         if (i % 2 == 0)
//         {
//             printf("%d\n", i);
//         }
//     }
//     return 0;
// }

// Q SMALLEST OF THREE NUMBERS

// #include <stdio.h>
// int nums(int a, int b, int c);
// int main()
// {
//     int a, b, c;
//     int type;
//     printf("Enter three numbers: \n");
//     scanf("%d%d%d", &a, &b, &c);
//     type = nums(a, b, c);
//     return type;
// }
// int nums(int a, int b, int c)
// {
//     if (a < b && a < c)
//     {
//         printf("%d is smaller than %d and %d", a, b, c);
//     }
//     else if (b < a && b < c)
//     {
//         printf("%d is smaller than %d and %d", b, a, c);
//     }
//     else if (c < a && c < b)
//     {
//         printf("%d is smaller than %d and %d", c, a, b);
//     }
//     return 0;
// }

// Q SUM OF SQUARES OF ODD NUMBERS

// #include <stdio.h>
// int main()
// {
//     int n;
//     scanf("%d", &n);
//     int sum = 0;
//     for (int i = 1; i <= n; i++)
//         sum += (2 * i - 1) * (2 * i - 1);
//     printf("%d", sum);
//     return 0;
// }

// Q TYPES OF TRIANGLES

// #include <stdio.h>
// int type_of_triangles(int a, int b, int c);
// int main()
// {
//     int a, b, c;
//     printf("Enter three sides of a triangle: \n");
//     scanf("%d%d%d", &a, &b, &c);
//     type_of_triangles(a, b, c);
//     return 0;
// }
// int type_of_triangles(int a, int b, int c)
// {
//     if (a == b && b == c && c == a)
//     {
//         printf("Equilateral triangle \n");
//     }
//     else if (a == b || b == c || c == a)
//     {
//         printf("Isoceles triangle \n");
//     }
//     else
//     {
//         printf("Scalene triangle \n");
//     }
//     return 0;
// }

// Q VOWELS OR CONSONANTS

// #include <stdio.h>
// int main()
// {
//     char ch;
//     printf("Enter the alphabet: ");
//     scanf("%c", &ch);
//     if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' ||
//         ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U')
//     {
//         printf("'%c' is Vowel.", ch);
//     }
//     else if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'))
//     {
//         printf("'%c' is Consonant.", ch);
//     }
//     else
//     {
//         printf("'%c' is not an alphabet.", ch);
//     }
//     return 0;
// }

/* PATTERN
1
12
123
1234
12345
*/

// #include <stdio.h>
// int main()
// {
//     int i, j, N = 5;
//     for (i = 1; i <= N; i++)
//     {
//         for (j = 1; j <= i; j++)
//         {
//             printf("%d", j);
//         }
//         printf("\n");
//     }
//     return 0;
// }

// Q  AVERAGE USING ARRAYS

// #include <stdio.h>
// int main()
// {
//     int n, i;
//     float nums[100], sum = 0.0, avg;
//     printf("Enter the number of elements: ");
//     scanf("%d", &n);
//     while (n > 100 || n < 1)
//     {
//         printf("Error, number should be in the range 1 to 100\n");
//         printf("Enter the number of elements: ");
//         scanf("%d", &n);
//     }
//     for (i = 0; i < n; ++i)
//     {
//         printf("%d. Enter number: ", i + 1);
//         scanf("%f", &nums[i]);
//         sum += nums[i];
//     }
//     avg = sum / n;
//     printf("Average = %.2f", avg);
//     return 0;
// }

// Q COUNT DUPLICATE ELEMENTS IN AN ARRAY

// #include <stdio.h>
// int main()
// {
//     int arr[100];
//     int i, j, size, count = 0;
//     printf("Enter the size of an array : ");
//     scanf("%d", &size);
//     for (i = 0; i < size; i++)
//     {
//         printf("%d. Enter number: ", i + 1);
//         scanf("%d", &arr[i]);
//     }
//     for (i = 0; i < size; i++)
//     {
//         for (j = i + 1; j < size; j++)
//         {
//             if (arr[i] == arr[j])
//             {
//                 count++;
//                 break;
//             }
//         }
//     }
//     printf("Total duplicated elements : %d", count);
//     return 0;
// }

// Q INTERCHANGING SMALLEST AND LARGEST ELEMENTS

// #include <stdio.h>
// int main()
// {
//     int n, i;
//     int s_pos, l_pos, temp;
//     int sm = 0, lrg = 0;
//     int arr[100];
//     printf("Enter the no of elems : ");
//     scanf("%d", &n);
//     printf("Enter the terms : \n");
//     for (i = 0; i < n; i++)
//     {
//         scanf("%d", &arr[i]);
//     }
//     printf("Initial array : \n");
//     for (i = 0; i < n; i++)
//     {
//         printf("%d\t", arr[i]);
//     }
//     printf("\n");
//     sm = arr[0];
//     for (i = 0; i < n; i++)
//     {
//         if (arr[i] <= sm)
//         {
//             sm = arr[i];
//             s_pos = i;
//         }
//         if (arr[i] >= lrg)
//         {
//             lrg = arr[i];
//             l_pos = i;
//         }
//     }
//     temp = arr[s_pos];
//     arr[s_pos] = arr[l_pos];
//     arr[l_pos] = temp;
//     printf("Final Array : \n");
//     for (i = 0; i < n; i++)
//     {
//         printf("%d \t", arr[i]);
//     }
//     return 0;
// }

// Q INSERTION OF ELEMENT IN ARRAY

// #include <stdio.h>
// int main()
// {
//     int i;
//     int arr[] = {1, 2, 3, 4, 5, 7, 8};
//     int n = 7;
//     int pos;
//     printf("Enter the position : ");
//     scanf("%d", &pos);
//     int val;
//     printf("Enter the value : ");
//     scanf("%d", &val);
//     for (i = n - 1; i >= pos - 1; i--)
//     {
//         arr[i] = arr[i + 1];
//     }
//     n++;
//     arr[pos - 1] = val;
//     for (i = 0; i < n; i++)
//     {
//         printf("%d \t", arr[i]);
//     }
// }

// Q INSERTION OF ELEMENT IN ARRAYAT THE END

// #include <stdio.h>
// int main()
// {
//     int arr[100], size, i, item;
//     printf("Enter the size : ");
//     scanf("%d", &size);
//     printf("Enter the elements \n");
//     for (i = 0; i < size; i++)
//     {
//         scanf("%d", &arr[i]);
//     }
//     printf("Original Array \n");
//     for (i = 0; i < size; i++)
//     {
//         printf("%d\t", arr[i]);
//     }
//     printf("\n");
//     printf("Enter the element to insert at the end : ");
//     scanf("%d", &item);
//     size = size + 1;
//     arr[size - 1] = item;
//     printf("Arrays after inserting last element \n");
//     for (i = 0; i < size; i++)
//     {
//         printf("%d\t", arr[i]);
//     }
// }

// Q DELETION OF AN ELEMENT IN ARRAY

// #include <stdio.h>
// int main()
// {
//     int arr[] = {2, 4, 6, 8, 9, 10, 12};
//     int pos, value, n, i;
//     n = 7;
//     printf("Enter the position : ");
//     scanf("%d", &pos);
//     printf("Enter the value : ");
//     scanf("%d", &value);
//     for (i = pos - 1; i < n - 1; i++)
//     {
//         arr[i] = arr[i + 1];
//     }
//     n--;
//     for (i = 0; i < n; i++)
//     {
//         printf("%d \t", arr[i]);
//     }
// }

// Q LINEAR SEARCH

// #include <stdio.h>
// int main()
// {
//     int n;
//     int i;
//     int arr[200];
//     printf("Enter no of elements : ");
//     scanf("%d", &n);
//     printf("Enter elements \n");
//     for (i = 0; i < n; i++)
//     {
//         scanf("%d", &arr[i]);
//     }
//     int k;
//     printf("Search for : ");
//     scanf("%d", &k);
//     for (i = 0; i < n; i++)
//     {
//         if (arr[i] == k)
//         {
//             printf("%d found at position %d ", k, i + 1);
//         }
//         else
//         {
//             printf("Cannot find %d", k);
//         }
//     }
// }

// Q BINARY SEARCH

// #include <stdio.h>
// int main()
// {
//     int i, low, high, mid, n, key, arr[100];
//     printf("Enter number of elements: ");
//     scanf("%d", &n);
//     printf("Enter %d integers \n", n);
//     for (i = 0; i < n; i++)
//         scanf("%d", &arr[i]);
//     printf("Search for: ");
//     scanf("%d", &key);
//     low = 0;
//     high = n - 1;
//     mid = (low + high) / 2;
//     while (low <= high)
//     {
//         if (arr[mid] == key)
//         {
//             printf("%d found at location %d \n", key, mid + 1);
//             break;
//         }
//         if (arr[mid] < key)
//         {
//             low = mid + 1;
//             mid = (low + high) / 2;
//         }
//         else
//         {
//             high = mid - 1;
//             mid = (low + high) / 2;
//         }
//     }
//     if (low > high)
//         printf("Cannot find %d in above array \n", key);
//     return 0;
// }

// Q (VLABS) ADDITION OF TWO MATRICES

// #include <stdio.h>
// int main()
// {
//     int a[100][100];
//     int b[100][100];
//     int n, m;
//     printf("No of rows : ");
//     scanf("%d", &n);
//     printf("No of columns : ");
//     scanf("%d", &m);
//     for (int i = 0; i < n; i++)
//     {
//         for (int j = 0; j < m; j++)
//         {
//             scanf("%d", &a[i][j]);
//         }
//     }
//     printf("\n");
//     for (int i = 0; i < n; i++)
//     {
//         for (int j = 0; j < m; j++)
//         {
//             scanf("%d", &b[i][j]);
//         }
//     }
//     int c[100][100];
//     for (int i = 0; i < n; i++)
//     {
//         for (int j = 0; j < m; j++)
//         {
//             c[i][j] = a[i][j] + b[i][j];
//         }
//     }
//     for (int i = 0; i < n; i++)
//     {
//         for (int j = 0; j < m; j++)
//         {
//             printf("%d\t", c[i][j]);
//         }
//         printf("\n");
//     }
// }

// Q HIGHEST ELEMENT IN COLUMN

// #include <stdio.h>
// int main()
// {
//     int n;
//     int m;
//     int arr[100][100];
//     printf("R : ");
//     scanf("%d", &n);
//     printf("C : ");
//     scanf("%d", &m);
//     for (int i = 0; i < n; i++)
//     {
//         for (int j = 0; j < m; j++)
//         {
//             printf("Enter the value for arr[%d][%d] : ", i, j);
//             scanf("%d", &arr[i][j]);
//         }
//         printf("\n");
//     }
//     for (int i = 0; i < n; i++)
//     {
//         for (int j = 0; j < m; j++)
//         {
//             printf("%d \t", arr[i][j]);
//         }
//         printf("\n");
//     }
//     int index = 1;
//     for (int i = 0; i < m; i++)
//     {
//         int max = arr[i][0];
//         for (int j = 0; j < n; j++)
//         {
//             if (arr[j][i] > max)
//             {
//                 max = arr[j][i];
//             }
//         }
//         printf("Highest among sub %d is %d \n", index, max);
//         index++;
//     }
// }

// Q 5 STATES AND 3 TYPES OF VACCINE

// #include <stdio.h>
// int main()
// {
//     int n, m, arr[100][100];
//     printf("Number of States : ");
//     scanf("%d", &n);
//     printf("Number of types of vaccine : ");
//     scanf("%d", &m);
//     for (int i = 0; i < n; i++)
//     {
//         for (int j = 0; j < m; j++)
//         {
//             printf("Enter the data of State %d, Vaccine %d : ", i + 1, j + 1);
//             scanf("%d", &arr[i][j]);
//         }
//         printf("\n");
//     }
//     for (int i = 0; i < n; i++)
//     {
//         for (int j = 0; j < m; j++)
//         {
//             printf("%d\t", arr[i][j]);
//         }
//         printf("\n");
//     }
//     printf("Total vaccine consumed in each state :- \n");
//     for (int i = 0; i < n; i++)
//     {
//         int sum = 0;
//         for (int j = 0; j < m; j++)
//         {
//             sum += arr[i][j];
//         }
//         printf("Total vaccine comsumed in State %d : %d \n", i + 1, sum);
//     }
//     printf("\n");
//     printf("Total consumption of each vaccine among all states :- \n");
//     for (int i = 0; i < m; i++)
//     {
//         int sum = 0;
//         for (int j = 0; j < n; j++)
//         {
//             sum += arr[j][i];
//         }
//         printf("Total consumption of vaccine %d : %d \n", i + 1, sum);
//     }
//     return 0;
// }

// Q DELETION OF WRONG REGISTERED MARKS

// #include <stdio.h>
// int main()
// {
//     int roll_no[] = {22, 34, 56, 115, 78};
//     int n = 5;
//     for (int i = 0; i < n; i++)
//     {
//         printf("%d\t", roll_no[i]);
//     }
//     printf("\n");
//     int k;
//     printf("Enter the position of an element to be deleted : ");
//     scanf("%d", &k);
//     for (int i = k - 1; i < n; i++)
//     {
//         roll_no[i] = roll_no[i + 1];
//     }
//     n--;
//     for (int i = 0; i < n; i++)
//     {
//         printf("%d\t", roll_no[i]);
//     }
//     return 0;
// }

// Q SORTING ALL PRIME NUMBERS FROM 1D ARRAY

// #include <stdio.h>
// int main()
// {
//     int n;
//     printf("Size of array : ");
//     scanf("%d", &n);
//     int arr[256];
//     printf("Enter array elements : \n");
//     for (int i = 0; i < n; i++)
//     {
//         printf(% d.Enter element % d:, i + 1, i + 1);
//         scanf("%d", &arr[i]);
//     }
//     for (int i = 0; i < n; i++)
//     {
//         printf("%d\t", arr[i]);
//     }
//     printf("\n");
//     for (int i = 0; i < n; i++)
//     {
//         int j = 2;
//         int p = 1;
//         while (j < arr[i])
//         {
//             if (arr[i] % j == 0)
//             {
//                 p = 0;
//                 break;
//             }
//             j++;
//         }
//         if (p == 1)
//         {
//             printf("%d\t", arr[i]);
//         }
//     }
//     return 0;
// }

// Q SWAPPING LARGEST AND SMALLEST ELEMENT FOR 2D ARRAY

// #include <stdio.h>
// int main()
// {
//     int r, c;
//     printf("ROWS : ");
//     scanf("%d", &r);
//     printf("COLUMNS : ");
//     scanf("%d", &c);
//     int arr[256][256];
//     printf("\n");
//     for (int i = 0; i < r; i++)
//     {
//         for (int j = 0; j < c; j++)
//         {
//             printf("Enter arr[%d][%d]: ", i, j);
//             scanf("%d", &arr[i][j]);
//         }
//         printf("\n");
//     }
//     printf("\n");
//     printf("Original Array \n");
//     for (int i = 0; i < r; i++)
//     {
//         for (int j = 0; j < c; j++)
//         {
//             printf("%d\t", arr[i][j]);
//         }
//         printf("\n");
//     }
//     printf("\n");
//     int min = arr[r - 1][c - 1];
//     int max = arr[0][0];
//     int maxr = 0;
//     int maxc = 0;
//     int minr = 0;
//     int minc = 0;
//     for (int i = 0; i < r; i++)
//     {
//         for (int j = 0; j < c; j++)
//         {
//             if (arr[i][j] > max)
//             {
//                 max = arr[i][j];
//                 maxr = i;
//                 maxc = j;
//             }
//             else if (arr[i][j] < min)
//             {
//                 min = arr[i][j];
//                 minr = i;
//                 minc = j;
//             }
//         }
//     }
//     int temp = arr[minr][minc];
//     arr[minr][minc] = arr[maxr][maxc];
//     arr[maxr][maxc] = temp;
//     printf("Array after swapping largest and smallest elements \n");
//     for (int i = 0; i < r; i++)
//     {
//         for (int j = 0; j < c; j++)
//         {
//             printf("%d\t", arr[i][j]);
//         }
//         printf("\n");
//     }
//     return 0;
// }

// Q BINARY SEARCH 1D ARRAY

// #include <stdio.h>
// int main()
// {
//     int marks[5] = {28, 32, 35, 37, 40};
//     int low = 0;
//     int high = 4;
//     int mid = low + high / 2;
//     while (low <= high)
//     {
//         if (marks[mid] == 37)
//         {
//             printf("37 found in marks for EEE \n");
//             break;
//         }
//         if (marks[mid] < 37)
//         {
//             low = mid + 1;
//             mid = low + high / 2;
//         }
//         else
//         {
//             high = mid - 1;
//             mid = low + high / 2;
//         }
//         if (low > high)
//         {
//             printf("Cannot find 37 in mraks \n");
//         }
//     }
//     return 0;
// }