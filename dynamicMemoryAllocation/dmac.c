#include <stdio.h>
#include <stdlib.h>

int main()
{
	// ? MALLOC
	// int *ptr;
	// int n;
	// printf("Enter the size of th array that you want to create\n");
	// scanf("%d", &n);
	// ptr = (int *)malloc(n * sizeof(int));
	// for (int i = 0; i < n; i++)
	// {
	// 	printf("Enter the value no %d of this array: ", i);
	// 	scanf("%d", &ptr[i]);
	// }
	// for (int i = 0; i < n; i++)
	// {
	// 	printf("Value no %d at this array is: %d\n", i, ptr[i]);
	// }

	// ? CALLOC
	// int *ptr;
	// int n;
	// printf("Enter the size of th array that you want to create: ");
	// scanf("%d", &n);
	// ptr = (int *)calloc(n, sizeof(int));
	// for (int i = 0; i < n; i++)
	// {
	// 	printf("Enter the value no %d of this array: ", i);
	// 	scanf("%d", &ptr[i]);
	// }
	// for (int i = 0; i < n; i++)
	// {
	// 	printf("Value no %d at this array is: %d\n", i, ptr[i]);
	// }

	// ? REALLOC
	int *ptr;
	int n;
	printf("Enter the size of th array that you want to create: ");
	scanf("%d", &n);
	ptr = (int *)calloc(n, sizeof(int));
	for (int i = 0; i < n; i++)
	{
		printf("Enter the value no %d of this array: ", i);
		scanf("%d", &ptr[i]);
	}
	for (int i = 0; i < n; i++)
	{
		printf("Value no %d at this array is: %d\n", i, ptr[i]);
	}
	printf("Enter the size of the new array: "); //* realloc
	scanf("%d", &n);
	ptr = (int *)realloc(ptr, n * sizeof(int));
	for (int i = 0; i < n; i++)
	{
		printf("Enter the value no %d of this array: ", i);
		scanf("%d", &ptr[i]);
	}
	for (int i = 0; i < n; i++)
	{
		printf("New value no %d at this array is: %d\n", i, ptr[i]);
	}
	free(ptr);
}
